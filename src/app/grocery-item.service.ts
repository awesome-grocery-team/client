import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GroceryItemService {

  // Constructors *****

  constructor(private http: HttpClient) { }

  // Functions *****
  getAllItems(): Observable<any>
  {
    return this.http.get('//localhost:8080/items');
  }
}
