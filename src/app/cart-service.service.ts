import { Injectable } from '@angular/core';
import { Globals} from './globals';

@Injectable({
  providedIn: 'root'
})
export class CartServiceService
{
  // Member Variables *****
  itemsInCart: any[] = new Array(0);

  // Constructor *****
  
  constructor(private global: Globals) { }

  // Functions *****
  
  addToCart(item): any
  {

    switch ( item.name )
    {
      case "Banana":
        this.itemsInCart.push(item.name);
        this.global.cartTotal += item.price;
        console.log("You pressed the banana");

        console.log("Your cart total: ", this.global.cartTotal);
        console.log("Your cart items (local itemsInCart): ", this.itemsInCart);
        break;

      case "Apple":
        this.itemsInCart.push(item.name);
        this.global.cartTotal += item.price;
        console.log("You pressed the apple!");

        console.log("Your cart total: ", this.global.cartTotal);
        console.log("Your cart items: ", this.itemsInCart);
        break;

      case "Kiwi":
        this.itemsInCart.push(item.name);
        this.global.cartTotal += item.price;
        console.log("You pressed the kiwi!");

        console.log("Your cart total: ", this.global.cartTotal);
        console.log("Your cart items: ", this.itemsInCart);
        break;

      case "Pineapple":
        this.itemsInCart.push(item.name);
        this.global.cartTotal += item.price;
        console.log("You pressed the pineapple!");

        console.log("Your cart total: ", this.global.cartTotal);
        console.log("Your cart items: ", this.itemsInCart);
        break;

      case "Grapes":
        this.itemsInCart.push(item.name);
        this.global.cartTotal += item.price;
        console.log("You pressed the grapes!");

        console.log("Your cart total: ", this.global.cartTotal);
        console.log("Your cart items: ", this.itemsInCart);
        break;

      case "Watermelon":
        this.itemsInCart.push(item.name);
        this.global.cartTotal += item.price;
        console.log("You pressed the watermelon!");

        console.log("Your cart total: ", this.global.cartTotal);
        console.log("Your cart items: ", this.itemsInCart);
        break;

      default:
        console.log("What item is that!!");
        break;
    }

    this.updateCartTotal();
  }


  removeFromCart(item)
  {
    for ( let i = 0; i< this.itemsInCart.length; i++ )
    {
      if ( this.itemsInCart[i].name === item.name )
      {
        this.itemsInCart.splice(i, 1);
      }
    }
  }


  // Adds up entire price of grocery list and updates the label.
  updateCartTotal()
  {
    document.getElementById("cart").innerHTML = "TOTAL AMOUNT: $" + this.global.cartTotal;
  }

}
