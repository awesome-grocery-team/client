import { Component, OnInit } from '@angular/core';
import {GroceryItemService} from "../grocery-item.service";
import {Globals} from "../globals";

@Component({
  selector: 'app-apples',
  templateUrl: './apples.component.html',
  styleUrls: ['./apples.component.css']
})
export class ApplesComponent implements OnInit {

  constructor(private groceryItemService: GroceryItemService, private global: Globals) {}


  ngOnInit() {
    if (this.global.groceryItems === undefined) {
      this.groceryItemService.getAllItems().subscribe(data => { this.global.groceryItems = data; });
    }
  }

}
