// Globals.ts

import { Injectable } from '@angular/core';

@Injectable()
export class Globals
{
  groceryItems: Array<any>;

  cartTotal: any = 0.0;
}
