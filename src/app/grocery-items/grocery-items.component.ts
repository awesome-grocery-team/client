import { Component, OnInit } from '@angular/core';
import {GroceryItemService} from '../grocery-item.service';
import { Globals } from '../globals';
import { CartServiceService } from '../cart-service.service';

@Component({
  selector: 'app-grocery-items',
  templateUrl: './grocery-items.component.html',
  styleUrls: ['./grocery-items.component.css'],
})
export class GroceryItemsComponent implements OnInit {
  
  // ***** Constructors *****

  constructor(private groceryItemService: GroceryItemService,
              private global: Globals,
              public cart_service: CartServiceService) { }

  ngOnInit()
  {
    this.groceryItemService.getAllItems().subscribe(data => { this.global.groceryItems = data; });
  }

  // ***** Functions *****

  addToCart(item): any
  {
    this.cart_service.addToCart(item);
  }

  removeFromCart(item)
  {
    for ( let i = 0; i< this.cart_service.itemsInCart.length; i++ )
    {
      if ( this.cart_service.itemsInCart[i].name === item.name )
      {
        this.cart_service.itemsInCart.splice(i, 1);
      }
    }
  }

  // Adds up entire price of grocery list and updates the label.
  updateCartTotal()
  {
    document.getElementById("cart").innerHTML = "TOTAL AMOUNT: $" + this.global.cartTotal;
  }

}
