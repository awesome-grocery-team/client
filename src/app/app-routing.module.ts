import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GroceryItemsComponent} from "./grocery-items/grocery-items.component";
import {BananasComponent} from "./bananas/bananas.component";
import {KiwisComponent} from "./kiwis/kiwis.component";
import {ApplesComponent} from "./apples/apples.component";

const routes: Routes = [
  { path: 'Home', component: GroceryItemsComponent},
  { path: 'Bananas', component: BananasComponent},
  { path: 'Kiwis', component: KiwisComponent},
  { path: 'Apples', component: ApplesComponent},
  { path: '', redirectTo: 'Home', pathMatch: 'full'},
  // { path: "", component: "bananas"},
  // { path: "", component: "bananas"},
  // { path: "", component: "bananas"},
  // { path: "", component: "bananas"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
