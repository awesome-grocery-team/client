import { Component, OnInit } from '@angular/core';
// import { DialogComponent} from '../dialog/dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit
{
  // MEMBER VARIABLES *****

  closedFlag: any = 0;

  constructor() { }

  // FUNCTIONS *****

  ngOnInit()
  {
  }

  printStuff()
  {
    console.log("YOU PRESSED ME :)");
  }

}
