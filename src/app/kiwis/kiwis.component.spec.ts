import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KiwisComponent } from './kiwis.component';

describe('KiwisComponent', () => {
  let component: KiwisComponent;
  let fixture: ComponentFixture<KiwisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KiwisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KiwisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
