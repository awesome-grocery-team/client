import { Component, OnInit } from '@angular/core';
import {Globals} from "../globals";
import {GroceryItemService} from "../grocery-item.service";

@Component({
  selector: 'app-kiwis',
  templateUrl: './kiwis.component.html',
  styleUrls: ['./kiwis.component.css']
})
export class KiwisComponent implements OnInit {

  constructor(private groceryItemService: GroceryItemService, private global: Globals) {}


  ngOnInit() {
    if (this.global.groceryItems === undefined) {
      this.groceryItemService.getAllItems().subscribe(data => { this.global.groceryItems = data; });
    }
  }

}
