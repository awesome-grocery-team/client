import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Globals } from './globals';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GroceryItemsComponent } from './grocery-items/grocery-items.component';
import {HttpClientModule} from '@angular/common/http';
import {GroceryItemService} from './grocery-item.service';
import { HeaderComponent } from './header/header.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { BananasComponent } from './bananas/bananas.component';
import { CartComponent } from './cart/cart.component';
import { KiwisComponent } from './kiwis/kiwis.component';
import { ApplesComponent } from './apples/apples.component';
import {CartServiceService} from './cart-service.service';

@NgModule({
  declarations: [
    AppComponent,
    GroceryItemsComponent,
    HeaderComponent,
    ItemCardComponent,
    BananasComponent,
    CartComponent,
    KiwisComponent,
    ApplesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    GroceryItemService,
    Globals,
    CartServiceService
  ],
  bootstrap: [
    AppComponent
  ]

// @NgModule({
//   declarations: [
//     AppComponent,
//     GroceryItemsComponent,
//     HeaderComponent,
//     ItemCardComponent,
//     BananasComponent,
//     CartComponent,
//     KiwisComponent,
//     DialogComponent,
//   ],
//   imports: [
//     BrowserModule,
//     AppRoutingModule,
//     HttpClientModule
//   ],
//   providers: [
//     GroceryItemService,
//     Globals,
//     DialogComponent
//   ],
//   bootstrap: [
//     AppComponent
//   ],
//   entryComponents: [
//     DialogComponent
//   ]
})
export class AppModule { }
