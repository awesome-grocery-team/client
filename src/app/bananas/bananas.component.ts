import { Component, OnInit } from '@angular/core';
import {Globals} from "../globals";
import { CartServiceService } from '../cart-service.service';

@Component({
  selector: 'app-bananas',
  templateUrl: './bananas.component.html',
  styleUrls: ['./bananas.component.css']
})
export class BananasComponent implements OnInit
{
  // Constructors *****

  constructor(private global: Globals,
              private cart_service: CartServiceService) {}

  ngOnInit()
  {

  }

  // Functions *****

  addToCart(item) : any
  {
    this.cart_service.addToCart(item);
  }

}
